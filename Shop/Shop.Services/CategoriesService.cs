﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.DataBase;
using Shop.Entities;

namespace Shop.Services
{
    public class CategoriesService
    {
        public void SaveCategory(Category category)
        {
            using (var context = new SHContext())
            {

                context.Categories.Add(category);
                context.SaveChanges();
            }
        }

        public List<Category> GetCategories()
        {
            using (var context = new SHContext())
            {
                return context.Categories.ToList();
            }
        }
        public Category GetCategory(int ID)
        {
            using (var context = new SHContext())
            {
                return context.Categories.Find(ID);
            }
        }
        public void UpdateCategory(Category category)
        {
            using (var context = new SHContext())
            {
                context.Entry(category).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteCategory(int ID)
        {
            using (var context = new SHContext())
            {
                var category = context.Categories.Find(ID);
                context.Categories.Remove(category);
                context.SaveChanges();
            }
        }

    }
}
