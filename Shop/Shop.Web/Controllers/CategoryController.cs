﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Entities;
using Shop.Services;

namespace Shop.Web.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        CategoriesService categoriesService = new CategoriesService();
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Category category)
        {
            categoriesService.SaveCategory(category);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Index()
        {
            var categories = categoriesService.GetCategories();
            return View(categories);
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
            var category = categoriesService.GetCategory(ID);
            return View(category);

        }
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            categoriesService.UpdateCategory(category);
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            var category = categoriesService.GetCategory(ID);
            return View(category);
        }
        [HttpPost]
        public ActionResult Delete(Category category)
        {
            category = categoriesService.GetCategory(category.ID);
            categoriesService.DeleteCategory(category.ID);
            return RedirectToAction("Index");
        }


    }
}